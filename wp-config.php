<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'tests_wp_timber' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost:3301' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}5ZBE,:_Ef+.5|Y9lk=}@ca#,p*in85]?ppRbyev/e;qHFR~v#c>f2faI6P+6(R ' );
define( 'SECURE_AUTH_KEY',  'Z-I$ts*7B@-u:;o-i^)tWiX{3V=:t{4+72Z|V%}[p0r6`|)@a.3503Kf[csZN#r.' );
define( 'LOGGED_IN_KEY',    'zHT^0<%nI9}gK)NW@ybNf8wh<jhM@awACPl6r;aS]|#D?$9*~]k=n+ X%Gvg?{lG' );
define( 'NONCE_KEY',        'Y.|yL2ATX+[)d^QT)Vi@^gpvF$PQbVmYx&SQg,-#3K>GcjG0%e^% <uEM#iz~#(=' );
define( 'AUTH_SALT',        '-WFz9VwwP0IJ){f$3m):3tM~Cm0/&@cv6`|=))_@Y5/,*Nky&8xJ{Ak1mFTu^,[9' );
define( 'SECURE_AUTH_SALT', 'sCNP-M}<jMZU=CeY/>A@sN7+nwj`%hL@J#eZPBd6-c:{n/OlsA *As8tNuXa<h94' );
define( 'LOGGED_IN_SALT',   'F8l(qOt{gm7CC|6B8QW6_nHF(kRx=Y0o|HxC1:XWTij[hm(?Ft ?E=+maPHfAw:)' );
define( 'NONCE_SALT',       'A|szae9bu;##N#sM%uHl7JL2:qSW$F$4XaUIUy.*MEgbIrsF[y5x$Q6JF86`8)YB' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
